package com.laughingpants.fantasicaalbum;

import com.laughingpants.fantasicaalbum.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.Window;

public class SplashActivity extends Activity {
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	
		
		if (android.os.Build.VERSION.SDK_INT >= 11) {
			getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
			getActionBar().hide();
		} else {
			getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		}
		
		setContentView(R.layout.activity_search);
		
		
	}
}
